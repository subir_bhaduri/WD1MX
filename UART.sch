EESchema Schematic File Version 4
LIBS:V1-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 7 12
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	4900 2475 5425 2475
Wire Wire Line
	4900 3125 5125 3125
Wire Wire Line
	4900 3025 5325 3025
Wire Wire Line
	4900 2925 4900 2475
Wire Wire Line
	5425 3275 5825 3275
Text GLabel 5825 3275 2    47   Input ~ 0
5V_uf
Wire Wire Line
	5425 2775 5825 2775
$Comp
L Device:Jumper_NC_Dual JP?
U 1 1 62647881
P 5425 3025
AR Path="/62647881" Ref="JP?"  Part="1" 
AR Path="/6262567D/62647881" Ref="JP?"  Part="1" 
AR Path="/626485ED/62647881" Ref="JP?"  Part="1" 
AR Path="/62AC4419/62647881" Ref="JP?"  Part="1" 
AR Path="/62B98D0E/62647881" Ref="JP?"  Part="1" 
AR Path="/62B98D10/62647881" Ref="JP?"  Part="1" 
AR Path="/62EFA7ED/62647881" Ref="JP31"  Part="1" 
AR Path="/62FDA237/62647881" Ref="JP33"  Part="1" 
AR Path="/62FF211C/62647881" Ref="JP35"  Part="1" 
F 0 "JP31" V 5379 3127 50  0000 L CNN
F 1 "Jumper_NC_Dual" V 5470 3127 50  0000 L CNN
F 2 "Jumper:SolderJumper-3_P1.3mm_Open_Pad1.0x1.5mm_NumberLabels" H 5425 3025 50  0001 C CNN
F 3 "~" H 5425 3025 50  0001 C CNN
	1    5425 3025
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 62647899
P 5425 2475
AR Path="/62647899" Ref="#PWR?"  Part="1" 
AR Path="/6262567D/62647899" Ref="#PWR?"  Part="1" 
AR Path="/626485ED/62647899" Ref="#PWR?"  Part="1" 
AR Path="/62AC4419/62647899" Ref="#PWR?"  Part="1" 
AR Path="/62B98D0E/62647899" Ref="#PWR?"  Part="1" 
AR Path="/62B98D10/62647899" Ref="#PWR?"  Part="1" 
AR Path="/62EFA7ED/62647899" Ref="#PWR030"  Part="1" 
AR Path="/62FDA237/62647899" Ref="#PWR032"  Part="1" 
AR Path="/62FF211C/62647899" Ref="#PWR034"  Part="1" 
F 0 "#PWR030" H 5425 2225 50  0001 C CNN
F 1 "GND" H 5430 2302 50  0000 C CNN
F 2 "" H 5425 2475 50  0001 C CNN
F 3 "" H 5425 2475 50  0001 C CNN
	1    5425 2475
	1    0    0    -1  
$EndComp
Text GLabel 5825 2775 2    47   Input ~ 0
3.3V
$Comp
L Connector:Conn_01x04_Male J?
U 1 1 626478A2
P 4700 3025
AR Path="/626478A2" Ref="J?"  Part="1" 
AR Path="/6262567D/626478A2" Ref="J?"  Part="1" 
AR Path="/626485ED/626478A2" Ref="J?"  Part="1" 
AR Path="/62AC4419/626478A2" Ref="J?"  Part="1" 
AR Path="/62B98D0E/626478A2" Ref="J?"  Part="1" 
AR Path="/62B98D10/626478A2" Ref="J?"  Part="1" 
AR Path="/62EFA7ED/626478A2" Ref="J18"  Part="1" 
AR Path="/62FDA237/626478A2" Ref="J19"  Part="1" 
AR Path="/62FF211C/626478A2" Ref="J20"  Part="1" 
F 0 "J18" H 4808 3306 50  0000 C CNN
F 1 "Conn_01x04_Male" H 4808 3215 50  0000 C CNN
F 2 "Connector_Molex:Molex_KK-254_AE-6410-04A_1x04_P2.54mm_Vertical" H 4700 3025 50  0001 C CNN
F 3 "~" H 4700 3025 50  0001 C CNN
	1    4700 3025
	1    0    0    -1  
$EndComp
Wire Wire Line
	5125 3125 5125 3750
Wire Wire Line
	5125 3750 5325 3750
Text HLabel 6850 3500 2    47   Output ~ 0
Rx
Text HLabel 5300 5275 2    47   Input ~ 0
Tx
$Comp
L Device:R R?
U 1 1 63023BE6
P 6375 4150
AR Path="/63023BE6" Ref="R?"  Part="1" 
AR Path="/62EFA7ED/63023BE6" Ref="R23"  Part="1" 
AR Path="/62FDA237/63023BE6" Ref="R25"  Part="1" 
AR Path="/62FF211C/63023BE6" Ref="R27"  Part="1" 
F 0 "R23" V 6275 4150 50  0000 C CNN
F 1 "5.1K" V 6375 4150 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 6305 4150 50  0001 C CNN
F 3 "~" H 6375 4150 50  0001 C CNN
	1    6375 4150
	-1   0    0    1   
$EndComp
$Comp
L Device:R R?
U 1 1 63023BEC
P 6375 4450
AR Path="/63023BEC" Ref="R?"  Part="1" 
AR Path="/62EFA7ED/63023BEC" Ref="R24"  Part="1" 
AR Path="/62FDA237/63023BEC" Ref="R26"  Part="1" 
AR Path="/62FF211C/63023BEC" Ref="R28"  Part="1" 
F 0 "R24" V 6250 4450 50  0000 C CNN
F 1 "10K" V 6375 4450 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 6305 4450 50  0001 C CNN
F 3 "~" H 6375 4450 50  0001 C CNN
	1    6375 4450
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 63023BF2
P 6375 4600
AR Path="/63023BF2" Ref="#PWR?"  Part="1" 
AR Path="/62EFA7ED/63023BF2" Ref="#PWR031"  Part="1" 
AR Path="/62FDA237/63023BF2" Ref="#PWR033"  Part="1" 
AR Path="/62FF211C/63023BF2" Ref="#PWR035"  Part="1" 
F 0 "#PWR031" H 6375 4350 50  0001 C CNN
F 1 "GND" H 6380 4427 50  0000 C CNN
F 2 "" H 6375 4600 50  0001 C CNN
F 3 "" H 6375 4600 50  0001 C CNN
	1    6375 4600
	1    0    0    -1  
$EndComp
Wire Wire Line
	6375 4300 6775 4300
Connection ~ 6375 4300
$Comp
L Device:Jumper_NC_Dual JP?
U 1 1 63023D5B
P 5425 3750
AR Path="/63023D5B" Ref="JP?"  Part="1" 
AR Path="/6262567D/63023D5B" Ref="JP?"  Part="1" 
AR Path="/626485ED/63023D5B" Ref="JP?"  Part="1" 
AR Path="/62AC4419/63023D5B" Ref="JP?"  Part="1" 
AR Path="/62B98D0E/63023D5B" Ref="JP?"  Part="1" 
AR Path="/62B98D10/63023D5B" Ref="JP?"  Part="1" 
AR Path="/62EFA7ED/63023D5B" Ref="JP32"  Part="1" 
AR Path="/62FDA237/63023D5B" Ref="JP34"  Part="1" 
AR Path="/62FF211C/63023D5B" Ref="JP36"  Part="1" 
F 0 "JP32" V 5379 3852 50  0000 L CNN
F 1 "Jumper_NC_Dual" V 5470 3852 50  0000 L CNN
F 2 "Jumper:SolderJumper-3_P1.3mm_Open_Pad1.0x1.5mm_NumberLabels" H 5425 3750 50  0001 C CNN
F 3 "~" H 5425 3750 50  0001 C CNN
	1    5425 3750
	0    1    1    0   
$EndComp
Wire Wire Line
	5425 3500 6775 3500
Wire Wire Line
	5425 4000 6375 4000
Wire Wire Line
	6775 4300 6775 3500
Connection ~ 6775 3500
Wire Wire Line
	6775 3500 6850 3500
Wire Wire Line
	4900 3225 4900 5275
Wire Wire Line
	4900 5275 5300 5275
$EndSCHEMATC
