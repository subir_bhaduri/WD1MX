EESchema Schematic File Version 4
LIBS:V1-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 6 12
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	4900 2475 5425 2475
Wire Wire Line
	4900 3125 5125 3125
Wire Wire Line
	4900 3025 5325 3025
Wire Wire Line
	4900 2925 4900 2475
Wire Wire Line
	5425 3275 5825 3275
Text GLabel 5825 3275 2    47   Input ~ 0
5V_uf
Wire Wire Line
	5425 2775 5825 2775
$Comp
L Device:Jumper_NC_Dual JP?
U 1 1 634B1EEC
P 5425 3025
AR Path="/634B1EEC" Ref="JP?"  Part="1" 
AR Path="/6262567D/634B1EEC" Ref="JP3"  Part="1" 
AR Path="/626485ED/634B1EEC" Ref="JP6"  Part="1" 
AR Path="/62AC4419/634B1EEC" Ref="JP?"  Part="1" 
AR Path="/62B98D0E/634B1EEC" Ref="JP25"  Part="1" 
AR Path="/62B98D10/634B1EEC" Ref="JP28"  Part="1" 
AR Path="/62EFA7ED/634B1EEC" Ref="JP?"  Part="1" 
F 0 "JP28" V 5379 3127 50  0000 L CNN
F 1 "Jumper_NC_Dual" V 5470 3127 50  0000 L CNN
F 2 "Jumper:SolderJumper-3_P1.3mm_Open_Pad1.0x1.5mm_NumberLabels" H 5425 3025 50  0001 C CNN
F 3 "~" H 5425 3025 50  0001 C CNN
	1    5425 3025
	0    1    1    0   
$EndComp
Wire Wire Line
	5425 4650 5825 4650
Wire Wire Line
	5425 4150 5825 4150
$Comp
L Device:Jumper_NC_Dual JP?
U 1 1 634B1EEE
P 5425 4400
AR Path="/634B1EEE" Ref="JP?"  Part="1" 
AR Path="/6262567D/634B1EEE" Ref="JP5"  Part="1" 
AR Path="/626485ED/634B1EEE" Ref="JP8"  Part="1" 
AR Path="/62AC4419/634B1EEE" Ref="JP?"  Part="1" 
AR Path="/62B98D0E/634B1EEE" Ref="JP27"  Part="1" 
AR Path="/62B98D10/634B1EEE" Ref="JP30"  Part="1" 
AR Path="/62EFA7ED/634B1EEE" Ref="JP?"  Part="1" 
F 0 "JP30" V 5379 4502 50  0000 L CNN
F 1 "Jumper_NC_Dual" V 5470 4502 50  0000 L CNN
F 2 "Jumper:SolderJumper-3_P1.3mm_Open_Pad1.0x1.5mm_NumberLabels" H 5425 4400 50  0001 C CNN
F 3 "~" H 5425 4400 50  0001 C CNN
	1    5425 4400
	0    1    1    0   
$EndComp
Wire Wire Line
	5425 4000 5825 4000
Wire Wire Line
	5425 3500 5825 3500
$Comp
L Device:Jumper_NC_Dual JP?
U 1 1 634B1EEF
P 5425 3750
AR Path="/634B1EEF" Ref="JP?"  Part="1" 
AR Path="/6262567D/634B1EEF" Ref="JP4"  Part="1" 
AR Path="/626485ED/634B1EEF" Ref="JP7"  Part="1" 
AR Path="/62AC4419/634B1EEF" Ref="JP?"  Part="1" 
AR Path="/62B98D0E/634B1EEF" Ref="JP26"  Part="1" 
AR Path="/62B98D10/634B1EEF" Ref="JP29"  Part="1" 
AR Path="/62EFA7ED/634B1EEF" Ref="JP?"  Part="1" 
F 0 "JP29" V 5379 3852 50  0000 L CNN
F 1 "Jumper_NC_Dual" V 5470 3852 50  0000 L CNN
F 2 "Jumper:SolderJumper-3_P1.3mm_Open_Pad1.0x1.5mm_NumberLabels" H 5425 3750 50  0001 C CNN
F 3 "~" H 5425 3750 50  0001 C CNN
	1    5425 3750
	0    1    1    0   
$EndComp
Text GLabel 5825 4650 2    47   BiDi ~ 0
SDA_5V
Text GLabel 5825 4000 2    47   BiDi ~ 0
SCL_5V
$Comp
L power:GND #PWR?
U 1 1 634B1EF0
P 5425 2475
AR Path="/634B1EF0" Ref="#PWR?"  Part="1" 
AR Path="/6262567D/634B1EF0" Ref="#PWR022"  Part="1" 
AR Path="/626485ED/634B1EF0" Ref="#PWR023"  Part="1" 
AR Path="/62AC4419/634B1EF0" Ref="#PWR?"  Part="1" 
AR Path="/62B98D0E/634B1EF0" Ref="#PWR028"  Part="1" 
AR Path="/62B98D10/634B1EF0" Ref="#PWR029"  Part="1" 
AR Path="/62EFA7ED/634B1EF0" Ref="#PWR?"  Part="1" 
F 0 "#PWR029" H 5425 2225 50  0001 C CNN
F 1 "GND" H 5430 2302 50  0000 C CNN
F 2 "" H 5425 2475 50  0001 C CNN
F 3 "" H 5425 2475 50  0001 C CNN
	1    5425 2475
	1    0    0    -1  
$EndComp
Text GLabel 5825 2775 2    47   Input ~ 0
3.3V
Text GLabel 5825 3500 2    47   BiDi ~ 0
SCL_3.3V
Text GLabel 5825 4150 2    47   BiDi ~ 0
SDA_3.3V
$Comp
L Connector:Conn_01x04_Male J?
U 1 1 634B1EF2
P 4700 3025
AR Path="/634B1EF2" Ref="J?"  Part="1" 
AR Path="/6262567D/634B1EF2" Ref="J10"  Part="1" 
AR Path="/626485ED/634B1EF2" Ref="J11"  Part="1" 
AR Path="/62AC4419/634B1EF2" Ref="J?"  Part="1" 
AR Path="/62B98D0E/634B1EF2" Ref="J16"  Part="1" 
AR Path="/62B98D10/634B1EF2" Ref="J17"  Part="1" 
AR Path="/62EFA7ED/634B1EF2" Ref="J?"  Part="1" 
F 0 "J17" H 4808 3306 50  0000 C CNN
F 1 "Conn_01x04_Male" H 4808 3215 50  0000 C CNN
F 2 "Connector_Molex:Molex_KK-254_AE-6410-04A_1x04_P2.54mm_Vertical" H 4700 3025 50  0001 C CNN
F 3 "~" H 4700 3025 50  0001 C CNN
	1    4700 3025
	1    0    0    -1  
$EndComp
Wire Wire Line
	5125 3125 5125 3750
Wire Wire Line
	5125 3750 5325 3750
Wire Wire Line
	4900 3225 4900 4400
Wire Wire Line
	4900 4400 5325 4400
$EndSCHEMATC
