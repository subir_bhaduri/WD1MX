EESchema Schematic File Version 4
LIBS:V1-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 10 12
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	4900 2475 5425 2475
Wire Wire Line
	4900 3125 5125 3125
Wire Wire Line
	4900 3025 5325 3025
Wire Wire Line
	4900 2925 4900 2475
Wire Wire Line
	5425 3275 5825 3275
Text GLabel 5825 3275 2    47   Input ~ 0
5V_uf
Wire Wire Line
	5425 2775 5825 2775
$Comp
L Device:Jumper_NC_Dual JP?
U 1 1 634B1EED
P 5425 3025
AR Path="/634B1EED" Ref="JP?"  Part="1" 
AR Path="/6262567D/634B1EED" Ref="JP?"  Part="1" 
AR Path="/626485ED/634B1EED" Ref="JP?"  Part="1" 
AR Path="/62AC4419/634B1EED" Ref="JP9"  Part="1" 
AR Path="/62AF4C8E/634B1EED" Ref="JP?"  Part="1" 
AR Path="/62B5C41E/634B1EED" Ref="JP13"  Part="1" 
AR Path="/62B706A3/634B1EED" Ref="JP17"  Part="1" 
AR Path="/62B706A6/634B1EED" Ref="JP21"  Part="1" 
AR Path="/62ABBA8F/634B1EED" Ref="JP10"  Part="1" 
AR Path="/62AD1624/634B1EED" Ref="JP11"  Part="1" 
AR Path="/62AE82E1/634B1EED" Ref="JP12"  Part="1" 
AR Path="/62B8E13D/634B1EED" Ref="JP13"  Part="1" 
F 0 "JP10" V 5379 3127 50  0000 L CNN
F 1 "Jumper_NC_Dual" V 5470 3127 50  0000 L CNN
F 2 "Jumper:SolderJumper-3_P1.3mm_Open_Pad1.0x1.5mm_NumberLabels" H 5425 3025 50  0001 C CNN
F 3 "~" H 5425 3025 50  0001 C CNN
	1    5425 3025
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 634B1EF1
P 5425 2475
AR Path="/634B1EF1" Ref="#PWR?"  Part="1" 
AR Path="/6262567D/634B1EF1" Ref="#PWR?"  Part="1" 
AR Path="/626485ED/634B1EF1" Ref="#PWR?"  Part="1" 
AR Path="/62AC4419/634B1EF1" Ref="#PWR024"  Part="1" 
AR Path="/62AF4C8E/634B1EF1" Ref="#PWR?"  Part="1" 
AR Path="/62B5C41E/634B1EF1" Ref="#PWR025"  Part="1" 
AR Path="/62B706A3/634B1EF1" Ref="#PWR026"  Part="1" 
AR Path="/62B706A6/634B1EF1" Ref="#PWR027"  Part="1" 
AR Path="/62ABBA8F/634B1EF1" Ref="#PWR026"  Part="1" 
AR Path="/62AD1624/634B1EF1" Ref="#PWR027"  Part="1" 
AR Path="/62AE82E1/634B1EF1" Ref="#PWR038"  Part="1" 
AR Path="/62B8E13D/634B1EF1" Ref="#PWR039"  Part="1" 
F 0 "#PWR038" H 5425 2225 50  0001 C CNN
F 1 "GND" H 5430 2302 50  0000 C CNN
F 2 "" H 5425 2475 50  0001 C CNN
F 3 "" H 5425 2475 50  0001 C CNN
	1    5425 2475
	1    0    0    -1  
$EndComp
Text GLabel 5825 2775 2    47   Input ~ 0
3.3V
Wire Wire Line
	5125 3125 5125 3750
Text GLabel 5725 3875 2    47   Input ~ 0
SPI_MOSI
$Comp
L Connector:Conn_01x06_Male J12
U 1 1 62AEFD29
P 4700 3125
AR Path="/62AC4419/62AEFD29" Ref="J12"  Part="1" 
AR Path="/62AF4C8E/62AEFD29" Ref="J?"  Part="1" 
AR Path="/62B5C41E/62AEFD29" Ref="J13"  Part="1" 
AR Path="/62B706A3/62AEFD29" Ref="J14"  Part="1" 
AR Path="/62B706A6/62AEFD29" Ref="J15"  Part="1" 
AR Path="/62ABBA8F/62AEFD29" Ref="J13"  Part="1" 
AR Path="/62AD1624/62AEFD29" Ref="J14"  Part="1" 
AR Path="/62AE82E1/62AEFD29" Ref="J15"  Part="1" 
AR Path="/62B8E13D/62AEFD29" Ref="J23"  Part="1" 
F 0 "J13" H 4808 3506 50  0000 C CNN
F 1 "Conn_01x06_Male" H 4808 3415 50  0000 C CNN
F 2 "Connector_Molex:Molex_KK-254_AE-6410-06A_1x06_P2.54mm_Vertical" H 4700 3125 50  0001 C CNN
F 3 "~" H 4700 3125 50  0001 C CNN
	1    4700 3125
	1    0    0    -1  
$EndComp
Text GLabel 5725 4000 2    47   Input ~ 0
SPI_SCK
Wire Wire Line
	4900 3225 5050 3225
Text HLabel 5725 4125 2    50   Input ~ 0
SPI_CS
Text HLabel 5725 3750 2    47   Output ~ 0
SPI_MISO
Wire Wire Line
	5125 3750 5725 3750
Wire Wire Line
	5050 3225 5050 3875
Wire Wire Line
	5050 3875 5725 3875
Wire Wire Line
	5000 3325 5000 4000
Wire Wire Line
	5000 4000 5725 4000
Wire Wire Line
	4900 3325 5000 3325
Wire Wire Line
	4900 3425 4900 4125
Wire Wire Line
	4900 4125 5725 4125
$EndSCHEMATC
