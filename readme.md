# Wemos D1 Mini Extension Board 

## Context / Why:
I realized I was using WemosD1 Mini in multiple projects. I like its availability and small size. It has great processing specs, and with inbuilt WiFi, its great for communications, if need be. However it has a very limited pin availability. I do not really like the ESP32s for my projects because of the various confusing varieties and confusing documentation. 

## My solution:
Create an extension board that expands on the basic available features of Wemos D1 Mini, using the I2C port expander IC - MCP23017.
- I2C pins are expanded to 4 I2C ports
- SPI pins are expanded to 4 SPI ports. The CS pins are operated via MCP23017 i2C expander.
- UART pins RX0 and TX0 (used for Serial0) are multiplexed to 4x UARTs, with 3x exposed as UART ports. Multiplexed via IC 4052.
- 8 GPIO pins are made available from the MCP23017.
- Its possible to connect 2x ADS1115 4 channel 16 bit ADCs, resulting in total 8x ADC channels.
- SIM800 - a versatile long distance communication module can be attached.
- RTC is added using DS3231
- SD card slot has been added for data storage.

# Issues in V1.0
- Footprint of IC4052 is wrong. Should changed to SOIC-16
- R14 and C15 names interchanged.
- C9 and C8 footprints too small (0603). Need footprints for 0805 or SMD Tantalum
- IC1 LM7805 footprint used is too unpopular! - Change it to popular Dpack.  
- SIM800 vertical installation clashes with Wemos D1 Mini !!
- Main issue: Wemos D1 Mini's RX0 is connected to the Serial USB chip's TX 
via a 400 ohm resistor. Thus, if RX0 is to be used as UART for other sensors, then it never
goes low. See issue here: 
https://electronics.stackexchange.com/questions/644753/how-to-bypass-strong-pullup-on-rx-pin-of-wemos-d1-mini-esp8266-to-communicate